###########################################################################################
###########################################################################################
##########                                                                    #############
##########          Position per�ue dans la distribution des revenus          #############
##########                et pr�f�rences pour la redistribution               #############
##########                                                                    #############
###########################################################################################
###########################################################################################

################################################################################
#
# Copyright (C) 2022. Logiciel �labor� par l'�tat, via la Drees.
#
# Nom de l'auteur : Rapha�l Lardeux, Drees.
#
# Ce programme informatique a �t� d�velopp� par la Drees. Il permet de produire 
# les figures publi�es dans l'�tudes et R�sultats 1234, intitul� � L'opinion des 
# Fran�ais sur les in�galit�s refl�te-t-elle leur position sur l'�chelle des revenus ?�,
# juin 2022. 
#
# Ce programme a �t� ex�cut� le 17/06/2022 avec la version 4.1.2 de R et les 
# packages : Hmisc_4.6-0, xlsx_0.6.5, MASS_7.3-55, xtable_1.8-4, sampler_0.2.4,
# KernSmooth_2.23-20, car_3.0-12, data.table_1.14.2,  haven_2.4.3.
#
# Le texte et les figures de l'�tude peuvent �tre consult�s sur le site de la 
# DREES : https://drees.solidarites-sante.gouv.fr/publications-communique-de-presse/etudes-et-resultats/lopinion-des-francais-sur-les-inegalites
# 
# Ce programme utilise les mill�simes 2014, 2016 et 2018 :
#   - du Barom�tre d'opinion de la Drees: https://drees.solidarites-sante.gouv.fr/sources-outils-et-enquetes/le-barometre-dopinion-de-la-drees
#   - des Enqu�tes sur les Revenus Fiscaux et Sociaux (ERFS - ):  https://data.progedo.fr/studies/doi/10.13144/lil-1440
#
# Bien qu'il n'existe aucune obligation l�gale � ce sujet, les utilisateurs de 
# ce programme sont invit�s � signaler � la DREES leurs travaux issus de la 
# r�utilisation de ce code, ainsi que les �ventuels probl�mes ou anomalies 
# qu'ils y rencontreraient, en �crivant � DREES-CODE@sante.gouv.fr
# 
# Ce logiciel est r�gi par la licence "GNU General Public License" GPL-3.0. 
# https://spdx.org/licenses/GPL-3.0.html#licenseText
# 
# � cet �gard l'attention de l'utilisateur est attir�e sur les risques associ�s
# au chargement, � l'utilisation, � la modification et/ou au d�veloppement et �
# la reproduction du logiciel par l'utilisateur �tant donn� sa sp�cificit� de 
# logiciel libre, qui peut le rendre complexe � manipuler et qui le r�serve donc 
# � des d�veloppeurs et des professionnels avertis poss�dant des connaissances 
# informatiques approfondies. Les utilisateurs sont donc invit�s � charger et 
# tester l'ad�quation du logiciel � leurs besoins dans des conditions permettant 
# d'assurer la s�curit� de leurs syst�mes et ou de leurs donn�es et, plus 
# g�n�ralement, � l'utiliser et l'exploiter dans les m�mes conditions de s�curit�.
# 
# Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez pris 
# connaissance de la licence GPL-3.0, et que vous en avez accept� les termes.
#
# This program is free software: you can redistribute it and/or modify it under 
# the terms of the GNU General Public License as published by the Free Software 
# Foundation, either version 3 of the License, or (at your option) any later 
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT 
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with 
# this program. If not, see <https://www.gnu.org/licenses/>.
#
################################################################################



rm(list=ls())


### Packages:
library(haven)
library(data.table)
library(car)
library(KernSmooth)
library(sampler)
library(xtable)
library(MASS)
library(xlsx)
library(Hmisc)

### Dossier de travail:
setwd("[chemin du dossier]")


### Localisation des donn�es:
path      <- "[chemin des donn�es du Barom�tre]"
path_erfs <- "[chemin des ERFS]"


### Charger les donn�es:
baro14 <- readRDS(paste0(path,"baro14.rds"))
baro16 <- readRDS(paste0(path,"baro16.rds"))
baro18 <- readRDS(paste0(path,"baro18.rds"))


### Fonction:
stars <- function(x){ifelse(x<=0.01,"***",ifelse(x>0.01 & x<=0.05,"**",ifelse(x>0.05 & x<=0.1,"*","")))}


### Cr�ation de dossiers:
dir.create("data", showWarnings = F)        # sauvegarde des bases temporaires
dir.create("excel", showWarnings = F)       # fichiers excel de sorties
dir.create("graphics", showWarnings = F)    # fichiers de graphiques R


### Seuils de revenu pour la position effective (ne faire tourner qu'une fois):
source("seuils_revenu.R")

# Sinon charger directement les codes:
load("data//seuils.Rdata")


### Traitement des donn�es:
source("data.R")

### Cr�ation du biais standardis�:
source("biais_standard.R")

### R�sultats de l'ER:
source("graphiques.R")
source("tableaux.R")
source("annexe_technique.R")






